data_01:
	curl -X POST localhost:5000/api/v1/posts \
	-H 'Content-Type: application/json' \
	-d @./data/data_01.json

data_02:
	curl -X POST localhost:5000/api/v1/posts \
	-H 'Content-Type: application/json' \
	-d @./data/data_02.json

data_03:
	curl -X POST localhost:5000/api/v1/posts \
	-H 'Content-Type: application/json' \
	-d @./data/data_03.json

search:
	curl "localhost:5000/api/v1/search?keyword=abcdefg&min_price=15000&max_price=25000"

search2:
	curl "localhost:5000/api/v1/search?keyword=ab"

test:
	curl -X POST localhost:5000/api/v1/posts \
	-H 'Content-Type: application/json' \
	-d @./data/test.json

setup:
	docker image rmi pg app -f
	docker build -t pg -f ./sql/Dockerfile .
	docker build -t app -f ./ecom/Dockerfile .
	docker network create -d bridge anglehealth
	docker run --name pg -p 5432:5432 --network anglehealth -d pg
	sleep 3;
	docker run --name app -p 5000:5000 --network anglehealth -d app 

stop:
	docker rm $$(docker ps -a -q --filter "ancestor=app") -f
	docker rm $$(docker ps -a -q --filter "ancestor=pg") -f
	docker network rm anglehealth
	docker image rmi app 
	docker image rmi pg