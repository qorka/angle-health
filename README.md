<h2 align="center">
  Angle Health
</h2>

## Setup
This application uses `Makefile` to run certain commands.

`make setup` will run a set of docker commands to dockerize the application and database.

`make stop` will stop all docker containers, remove images, and remove the created network.

## Data

`make data_nn` will <i>curl<i> the `posts` endpoint with the respective data in the `data` directory. (ie: `make data_01` will send a post request with ./data/data_01.json)

## Other

`make search` will curl the `search` endpoint.