from psycopg2 import connect, Error
from psycopg2.extras import execute_values, RealDictCursor

from flask import Flask, Response, request, jsonify
from config import pg, err_msgs

app = Flask(__name__)
conn = connect(**pg)

@app.route('/api/v1/posts', methods=['POST'])
def products():
  req = request.json['posts']
  columns = req[0].keys()
  values = [[value for value in product.values()] for product in req]

  try:
    cur = conn.cursor(cursor_factory=RealDictCursor)

    query = f"INSERT INTO products ({', '.join(columns)}) VALUES %s"

    execute_values(cur, query, values)
    conn.commit()

    return 201
  
  except Error as e:
    conn.rollback()

    start = str(e).find('(')
    end = str(e).find(').')

    return jsonify({'Message': err_msgs[e.pgcode](str(e)[start:end])}), 422

  except Exception:
    return jsonify({'Message': 'Invalid structure'}), 400

@app.route('/api/v1/search', methods=['GET'])
def search():
  try:
    keyword = request.args.get('keyword').lower()
    min_price = request.args.get('min_price', 0)
    max_price = request.args.get('max_price', None)

    cur = conn.cursor(cursor_factory=RealDictCursor)

    if max_price:
      query = f"""
        SELECT * 
        FROM products
        WHERE name ILIKE '%%' || %(keyword)s || '%%' AND
        price >= %(min_price)s AND
        price <= %(max_price)s
      """

      cur.execute(query, { 'keyword': keyword, 'min_price': min_price, 'max_price': max_price })

    else:
      query = f"""
        SELECT * 
        FROM products
        WHERE name ILIKE '%%' || %(keyword)s || '%%' AND
        price >= %(min_price)s 
      """

      cur.execute(query, { 'keyword': keyword, 'min_price': min_price })

    results = cur.fetchall()

    return jsonify({ 'posts': results }), 200

  except AttributeError:
    return jsonify({'Message': 'Missing keyword parameter.'}), 400

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=5000)