# postgres data
pg = {
  'host': 'pg',
  'database': 'anglehealth',
  'user': 'admin',
  'password': 'postgres',
  'port': 5432
}

err_msgs = {
  '23505': lambda x: f'Name already exists. ({x})',
  '23514': lambda x: f'Date must be in the future. ({x})'
}

# mongo data
mongodb = {
  'db': 'anglehealth',
  'user': 'anglehealth',
  'pw': 'notagoodpw',
  'uri': 'cluster0.9rrx1.mongodb.net'}

validator = { '$jsonSchema': 
  { 'bsonType': 'object',
    'required': ['name', 'price', 'start_date'],
    'properties': {
      'name': { 
        'bsonType': 'string',
        'pattern': '^[A-Za-z0-9][A-Za-z0-9\s\-]{4,10}$',
        'uniqueItems': True,
        'description': """Name for the product, 4-10 characters long.The  first letter has to be a digit or a letter (0-9, A-Z, a-z). All other letters has to be a digit, a letter, a space, or a hyphen (0-9, A-Z, a-z, , -)."""},
      'price': {
        'bsonType': 'int',
        'description': """Price for the product, an integer representing the price in cents. E.g., 10000 means 10000 cents and therefore 100 dollars."""},
      'start_date': {
        'bsonType': 'string',
        'pattern': '^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$',
        # 'gte': 'new Date("01/01/2020")',
        'description': """Product sale start date, in the form of MM/DD/YYYY."""}
      }
    }
  }
